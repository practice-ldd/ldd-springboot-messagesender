package springcloud.service.sender;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

@SpringBootApplication
@EnableEurekaClient
@EnableHystrix
@EnableHystrixDashboard
//@ComponentScan(basePackages = {"com.demo2do","com.suidifu", "com.zufangbao"})
public class MessageSenderServerApplication {

    public static void main(String[] args) {

        SpringApplication.run(MessageSenderServerApplication.class, args);

    }
}
