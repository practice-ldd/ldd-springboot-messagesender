package springcloud.service.sender.config;

import javax.annotation.Resource;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @Auther: liudong
 * @Date: 18-10-30 16:03
 * @Description:
 */
@Configuration
public class Config {

    /**
     * 连接工厂
     */
    @Resource
    private ConnectionFactory connectionFactory;

    @Bean
    @LoadBalanced
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean("rabbitTemplate")
    public RabbitTemplate rabbitMQTemplate(){
        return  new RabbitTemplate(connectionFactory);
    }
}
