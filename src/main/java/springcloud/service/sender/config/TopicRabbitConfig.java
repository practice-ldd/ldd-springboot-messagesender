package springcloud.service.sender.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Auther: liudong
 * @Date: 19-1-24 12:21
 * @Description:
 */
@Configuration
public class TopicRabbitConfig {
    final static String message = "topic.message";
    final static String messages = "topic.messages";

    @Bean
    public Queue queueMessage() {
        return new Queue(message);
    }

    @Bean
    public Queue queueMessages() {
        return new Queue(messages);
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange("exchange");
    }

    @Bean
    Binding bindingExchangeMessage(TopicExchange exchange) {
        return BindingBuilder.bind(queueMessage()).to(exchange).with("topic.message");
    }

    @Bean
    Binding bindingExchangeMessages(TopicExchange exchange) {
        return BindingBuilder.bind(queueMessages()).to(exchange).with("topic.#");
    }

}
