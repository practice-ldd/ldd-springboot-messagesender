package springcloud.service.sender.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Auther: liudong
 * @Date: 19-1-23 19:00
 * @Description:
 */
@Configuration
public class SenderConfig {
    @Bean
    public Queue queueMessage() {
        return new Queue("first_queue");
    }
    @Bean
    TopicExchange exchange() {
        return new TopicExchange("first_exchange");
    }
    @Bean
    Binding bindingExchangeMessage(TopicExchange exchange) {
        return BindingBuilder.bind(queueMessage()).to(exchange).with("queue-first_queue");
    }
}
