package springcloud.service.sender.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import javax.annotation.Resource;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Raye on 2017/5/22.
 */
@RestController
@RequestMapping(value = "/message/sender")
public class Controller {
    @Resource
    private RabbitTemplate rabbitTemplate;

    @GetMapping(value = "/queue1")
    public void send(int i) {
        rabbitTemplate.convertAndSend("first_exchange","first_queue",":hello,rabbit~ :"+i);
    }

    public void send1() {
        String context = "hi, i am message 1";
        System.out.println("Sender : " + context);
        this.rabbitTemplate.convertAndSend("exchange", "topic.message", context);
    }

    public void send2() {
        String context = "hi, i am messages 2";
        System.out.println("Sender : " + context);
        this.rabbitTemplate.convertAndSend("exchange", "topic.messages", context);
    }
}