package springcloud.service.sender.controller;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Auther: liudong
 * @Date: 19-1-24 11:11
 * @Description:
 */
@Component


public class HelloReceiver {

    @RabbitHandler
    @RabbitListener(queues = "first_queue")
    public void process(String hello) {
        System.out.println("Receiver  : 1" + hello);
    }
//    @RabbitHandler
//    @RabbitListener(queues = "first_queue")
//    public void process1(String hello) {
//        System.out.println("Receiver  : 2" + hello);fgdgd
//    }
//    @RabbitHandler
//    @RabbitListener(queues = "first_queue")
//    public void process2(String hello) {
//        System.out.println("Receiver  : 3" + hello);
//    }
}
