package springcloud.service.sender;

/**
 * @Auther: liudong
 * @Date: 18-8-22 10:17
 * @Description:
 */

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MessageSenderServerApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Transactional()
//@TestPropertySource(locations = "classpath:bootstrap-junit-test.properties")
public abstract class BaseSpringbootTest {
}
