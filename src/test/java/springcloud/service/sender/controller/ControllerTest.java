package springcloud.service.sender.controller;


import com.sun.jersey.core.impl.provider.entity.XMLJAXBElementProvider.App;
import javax.annotation.Resource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import springcloud.service.sender.BaseSpringbootTest;
import springcloud.service.sender.MessageSenderServerApplication;

/**
 * @Auther: liudong
 * @Date: 19-1-23 19:03
 * @Description:
 */
public class ControllerTest extends BaseSpringbootTest {
    @Resource
    private Controller helloSender;

    @Test
    public void testRabbit() {
        helloSender.send(1);
    }


    @Test
    public void oneToMany() throws Exception {
        for (int i=0;i<50;i++){
            helloSender.send(i);
        }
    }

}